from glob import glob

import cv2
import numpy as np
import torch
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms

SIZE = 224
data_cat = ['train', 'val']


class Clahe():
    def __init__(self, clip=2.0, gridSize=(8,8)):
        self.clip = clip
        self.gridSize = gridSize

    def __call__(self, img):
        clahe = cv2.createCLAHE(self.clip, self.gridSize)
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        hsv[:,:,2] = clahe.apply(hsv[:,:,2])
        return cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)


class Generator(Dataset):
    def __init__(self, path, phase, transform, crop_count):
        self.phase = phase
        self.crop_count = crop_count
        if self.phase == 'train':
            self.data = glob(path + '/*/*.JPG')
        else:
            self.data = glob(path + '/*/*/*.JPG')
        self.transform = transform

    def __len__(self):
        return len(self.data)

    def generate(self, img, count):
        subset = []
        for i in range(count):
            x = np.random.randint(0, int(img.shape[0] / 3))
            y = np.random.randint(0, int(img.shape[1] / 3))
            dx = 2 * int(img.shape[0] / 3)
            dy = 2 * int(img.shape[1] / 3)
            img_ = img[x:x+dx, y:y+dy]
            subset.append(self.transform(img_))
        return subset

    def __getitem__(self, idx):
        img = cv2.imread(self.data[idx])
        label = int(self.data[idx].split('\\')[1]) -1
        if self.phase == 'train':
            subset = self.generate(img, self.crop_count)
            return torch.stack(subset), torch.tensor([label] * self.crop_count)
        else:
            img = self.transform(img)
            return img, label

def load_data(batch_size=16, crop_count=10):
    img_path = 'ML'

    transform = {
        'train': transforms.Compose([
            Clahe(),
            transforms.ToPILImage(),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.RandomRotation(45),
            transforms.Resize((SIZE, SIZE)),
            transforms.ToTensor(),
        ]),
        'val': transforms.Compose([
            Clahe(),
            transforms.ToPILImage(),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.RandomRotation(45),
            transforms.Resize((SIZE, SIZE)),
            transforms.ToTensor(),
        ])
    }

    image_datasets = {x: Generator(img_path, x, transform[x], crop_count)
                    for x in data_cat}

    dataLoader = {x: DataLoader(image_datasets[x], batch_size=batch_size,
                                shuffle=True, num_workers=6)
                for x in data_cat}

    return image_datasets, dataLoader
