import torch
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np

from generator import load_data

data_cat = ['train', 'val']


def  plot_inputs(inputs):
    fig = plt.figure(figsize=(30,30))
    for i in range(inputs.shape[0]):
        img = inputs[i, ...].transpose(1,2,0)[...,::-1]
        fig.add_subplot(10, np.ceil(inputs.shape[0] / 10), i+1)
        plt.imshow(img)
    plt.show()


def train(model, optim, criterion, epochs, batch_size, crop_count):
    image_datasets, dataloaders = load_data(batch_size=batch_size,
                                            crop_count=crop_count)
    data_size = {x: len(image_datasets[x]) for x in data_cat}

    min_loss = 1e5
    acc = 0

    for epoch in range(epochs):
        print(f'Epoch {epoch+1}/{epochs}')
        print('-' * 10)

        for phase in data_cat:
            if phase == 'train':
                model.train()
            else:
                model.eval()

            run_loss = 0.0
            run_correct = 0

            with tqdm(dataloaders[phase]) as t:
                for inputs, labels in t:
                    inputs = inputs.cuda()
                    labels = labels.cuda()
                    inputs = inputs.view(-1,3,224,224)
                    labels = labels.view(-1)

                    plot_inputs(inputs.cpu().numpy())

                    optim.zero_grad()

                    with torch.set_grad_enabled(phase == 'train'):
                        outputs = model(inputs)
                        loss = criterion(outputs, labels)

                        if phase == 'train':
                            loss.backward()
                            optim.step()

                    pred = torch.argmax(outputs, dim=1)
                    run_loss += loss.item()
                    run_correct += torch.sum(pred == labels)
                if phase == 'train':
                    epoch_corr = run_correct.item() / (data_size[phase] * crop_count)
                else:
                    epoch_corr = run_correct.item() / data_size[phase]
                epoch_loss = run_loss / data_size[phase]
                print(f'Loss: {epoch_loss:.4f} \nAcc: {epoch_corr:.4f}')

                # if phase == 'val' and epoch_loss < min_loss:
                if phase == 'val' and epoch_corr > acc:
                    min_loss = epoch_loss
                    acc = epoch_corr
                    torch.save(model.state_dict(), 'checkpoint/гы.pth')
                print(f'Min Loss: {min_loss:.4f}\nAcc: {acc:.4f}')

        print()

    print(f'Minimal Loss: {min_loss:.4f}\nAcc: {acc:.4f}')
